import os
import sys
from datetime import date
import time
import json
from pygame import mixer

import config
import Receiver
import LightsSounds

# We are importing 'Flask' to create an application,
# 'render_template' to render the given template as the response

from flask import Flask, render_template, jsonify, request, Response
from functools import wraps

# Get script path to use for Status Variables & Relative location
script_path = os.path.dirname(os.path.realpath(sys.argv[0]))

# Create a flask app and set a random secret key
app = Flask(__name__)
app.secret_key = os.urandom(24)
app.config['TEMPLATES_AUTO_RELOAD'] = True

# Secure the flask app with basic authentication
app.config['BASIC_AUTH_USERNAME'] = config.basicauth_user
app.config['BASIC_AUTH_PASSWORD'] = config.basicauth_pass


def is_local_network():
    if "192.168.1" in request.remote_addr:
        return True
    else:
        return False


# Define our authentication & response functions
def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    correct_username = app.config['BASIC_AUTH_USERNAME']
    correct_password = app.config['BASIC_AUTH_PASSWORD']
    return username == correct_username and password == correct_password


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response('Could not verify your access level for that URL.\n'
                    'You have to login with proper credentials', 401,
                    {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if is_local_network():
            print('Local Network Detected - Skip Auth')
            return f(*args, **kwargs)
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated


# ------------------------------------------------------------------------------
# GETTERS / SETTERS FOR LIGHTS AND SOUND CONTROLS
# ------------------------------------------------------------------------------

def readFrontEnd():
    front_end_file = script_path + '/status/front_end_info'
    with open(front_end_file) as data_file:
        game_info = json.load(data_file)
    return game_info


def readFrontEndNew():
    front_end_file = script_path + '/status/front_end_info_new'
    with open(front_end_file) as data_file:
        game_info_new = json.load(data_file)
    return game_info_new


def getNextGame():
    today = date.today()
    future = date(2016, 10, 13)
    next_game_info = []
    next_game_info.append((future - today).days)            # 0 - Countdown in Days
    next_game_info.append(future)                           # 1 - Next Game Date
    next_game_info.append("@ Panthers")                     # 2 - Team Being Played
    return next_game_info


def getLightsStatus():
    return os.path.isfile(config.filepath + '/python/nhlgoallight/status/lights')


def setLightStatus(status):
    filename = config.filepath + '/python/nhlgoallight/status/lights'
    if (status == "enable"):
        open(filename, 'w').close()
    else:
        if os.path.exists(filename):
            os.remove(filename)


def getScriptStatus():
    filename = config.filepath + '/python/nhlgoallight/status/status.pid'
    if os.path.isfile(filename):
        pidfile = open(filename, 'r')
        pid = pidfile.readline()
        pidfile.close()

        try:
            os.kill(int(pid), 0)
        except OSError:
            return False
        else:
            return True
    else:
        return False


def getSoundStatus():
    return os.path.isfile(config.filepath + '/python/nhlgoallight/status/sounds')


def setSoundStatus(status):
    filename = config.filepath + '/python/nhlgoallight/status/sounds'
    if (status == "enable"):
        open(filename, 'w').close()
    else:
        if os.path.exists(filename):
            os.remove(filename)


def setSoundTestStatus(status):
    if status == "enable":
        mixer.init()
        # Receiver.set_power_state("ON")
        Receiver.set_sd_mode("ANALOG")
        # Receiver.send_telnet("SDANALOG")
        time.sleep(1)
        Receiver.set_volume(60)
        mixer.music.load(config.filepath + '/python/nhlgoallight/Devils2015GoalSong.mp3')
        mixer.music.play()

    else:
        Receiver.set_sd_mode("AUTO")
        mixer.music.stop()


def setManualGoalDevils():
    filename = config.filepath + '/python/nhlgoallight/status/goalDevils'
    open(filename, 'w').close()


# / route and its view. This renders the index.html template
@app.route('/')
@requires_auth
def index():
    print('Local Network - ', is_local_network())
    print('Script Status - ', getScriptStatus())
    print('Lights Status - ', getLightsStatus())
    print('Sound Status  - ', getSoundStatus())
    print('Game Info -', readFrontEnd())
    print('Next Game - ', getNextGame())
    print('Authorized    - True')
    return render_template('indexbyteam.html',
                           vReceiverPower=Receiver.get_power_state(), vReceiverVolume=Receiver.get_volume(),
                           vNextGame=getNextGame(), vGameInfo=readFrontEnd(), vGameInfoNew=readFrontEndNew(),
                           vScriptStatus=getScriptStatus(), vLightStatus=getLightsStatus(), vSoundStatus=getSoundStatus(), vLocalNetwork=is_local_network())


@app.route('/admin')
@requires_auth
def admin():
    print('Receiver Power - ', Receiver.get_power_state())
    print('Receiver Volume - ', Receiver.get_volume())
    print('Script Status - ', getScriptStatus())
    print('Lights Status - ', getLightsStatus())
    print('Sound Status  - ', getSoundStatus())
    print('Authorized    - True')
    return render_template('admin.html',
                           vReceiverPower=Receiver.get_power_state(), vReceiverVolume=Receiver.get_volume(),
                           vScriptStatus=getScriptStatus(), vLightStatus=getLightsStatus(), vSoundStatus=getSoundStatus())


@app.route('/debug')
@requires_auth
def debug():
    return render_template('debug.html')


# ------------------------------------------------------------------------------
# ROUTES FOR CHANGING LIGHTS & SOUNDS STATUS
# ------------------------------------------------------------------------------


@app.route('/lights/<status>', methods=['POST'])
def changeLightStatus(status):
    print('Lights Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_lights:
        print('Passowrd verified - changing Light Status!')
        print('Lights Status Request - ', status)
        setLightStatus(status)
        return jsonify(newStatus=status)
    else:
        print('Incorrect Password - not changing Light Status!')
        return jsonify(newStatus=getLightsStatus())


@app.route('/lights/test/<status>', methods=['POST'])
def changeLightTestStatus(status):
    print('Lights Test Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_lights:
        print('Passowrd verified - changing Light Test Status!')
        print('Lights Test Status Request - ', status)
        LightsSounds.goalLights(status)
        return jsonify(newStatus=status)
    else:
        print('Incorrect Password - not changing Light Status!')
        return jsonify(newStatus=getLightsStatus())


@app.route('/sounds/<status>', methods=['POST'])
def changeSoundStatus(status):
    print('Sounds Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_sounds:
        print('Passowrd verified - changing Sound Status!')
        print('Sounds Status Request - ', status)
        setSoundStatus(status)
        return jsonify(newStatus=status)
    else:
        print('Incorrect Password - not changing Sounds Status!')
        return jsonify(newStatus=getSoundStatus())


# ------------------------------------------------------------------------------
# ROUTES FOR TESTING LIGHTS & SOUNDS
# ------------------------------------------------------------------------------


@app.route('/tests/lights', methods=['POST'])
def performTestsLights():
    print('Full Test Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_test:
        print('Passowrd verified - Testing Lights!')

        LightsSounds.goalLights("enable")
        time.sleep(config.goal_time)
        LightsSounds.goalLights("disable")
        # GPIO.cleanup()

        return True
    else:
        print('Incorrect Password - NOT Testing Lights & Sounds!')
        return False


@app.route('/tests/sounds', methods=['POST'])
def performTestsSounds():
    print('Sound Test Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_test:
        print('Passowrd verified - Testing Sounds!')

        if(Receiver.get_power_state() == 'STANDBY'):
            powerFlipped = True
            Receiver.set_power_state('ON')
            time.sleep(1)

        LightsSounds.goalHorn("enable")
        Receiver.set_volume(30)
        time.sleep(config.goal_time)
        LightsSounds.goalHorn("disable")
        # GPIO.cleanup()

        if powerFlipped is True:
            Receiver.set_power_state('OFF')

        return True
    else:
        print('Incorrect Password - NOT Testing Lights & Sounds!')
        return False


@app.route('/tests/full', methods=['POST'])
def performTestsFull():
    print('Full Test Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_test:
        print('Passowrd verified - Testing Lights & Sounds!')

        if Receiver.get_power_state() == 'STANDBY':
            powerFlipped = True
            print('Receiver was off - powerFlipped is', powerFlipped)
            Receiver.set_power_state('ON')
            time.sleep(2)

        LightsSounds.goalHorn('enable')
        LightsSounds.goalLights('enable')

        time.sleep(config.goal_time)
        LightsSounds.goalHorn('disable')
        LightsSounds.goalLights('disable')
        # GPIO.cleanup()

        if powerFlipped is True:
            Receiver.set_power_state('OFF')
            print('Receiver was off - turning it back off now...')

        return jsonify(success=True), 200

    else:
        print('Incorrect Password - NOT Testing Lights & Sounds!')
        return jsonify(success=False), 401


# ------------------------------------------------------------------------------
# ROUTES FOR SETTING DEVILS GOAL STATUS
# ------------------------------------------------------------------------------


@app.route('/devilsscored', methods=['POST'])
def devils_scored():
    if request.form['password'] == config.post_score:
        print('Passowrd verified - setting Devils Scored!')
        setManualGoalDevils()
        return jsonify(scored=True)
    else:
        print('Incorrect Password - not setting Devils Scored!')
        return jsonify(scored=False)


if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("10101")
    )
