#!/usr/local/bin/python3

import threading
import time
import datetime
import os
import sys

import logging  # for the following line
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)  # suppress IPV6 warning on startup
from scapy.all import *
from scapy.utils import *

# Custom Imports
import config
import LightsSounds

# it takes a minute for the scapy sniffing to initialize, so I print this to know when it's actually ready to go
print('Initialization done - checking local packets.')

# Define some Global Variables
lasttime = time.time()
print("[DEBUG] Last Time Set -", lasttime)

# Get script path to use for Status Variables & Relative location
script_path = os.path.dirname(os.path.realpath(sys.argv[0]))


def setButtonGoalStatus():
    filename = script_path + '/status/goalDevilsButton'
    open(filename, 'w').close()
    os.chmod(filename, 0o777)


def lights_and_sounds():
	LightsSounds.goalHorn('enable')
	LightsSounds.goalLights('enable')

	time.sleep(config.goal_time)
	LightsSounds.goalHorn('disable')
	LightsSounds.goalLights('disable')


def button_pressed(time):
    global lasttime
    diff = time - lasttime

    print("[MAIN] Last press (seconds ago) - ", int(diff))

    if (diff > 45):
        lasttime = time
        print("[MAIN] Dash Button Pressed & Time Elapsed!")
        setButtonGoalStatus()
        thread = threading.Thread(target = lights_and_sounds, args=[])
        thread.start()
        # lights_and_sounds()
    else:
        # DEBUG
        print("[MAIN] Button pressed too quickly - ignoring!")

    print("--------------------------------")


def udp_filter(pkt):
    # print("[DEBUG] Packet Sniffed @", datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'))
    options = pkt[DHCP].options
    for option in options:
        if isinstance(option, tuple):
            if 'requested_addr' in option:
                # we've found the IP address, which means its the second and final UDP request, so we can trigger our action
                if pkt.src == config.dash_button_mac:
                    print("[DEBUG] Packet MAC Address -", pkt.src)
                    print("[DEBUG] Dash MAC Address -", config.dash_button_mac)
                    print("[DEBUG] Packet is a Dash Button - trigger goal!")
                    button_pressed(time.time())
                    break
                    print("[DEBUG] AFTER BREAK - SHOULD NEVER SEE THIS LINE OF CODE!")


mac_to_action = {config.dash_button_mac : button_pressed}
mac_id_list = list(mac_to_action.keys())

sniff(prn=udp_filter, store=0, filter="udp", lfilter=lambda d: d.src in mac_id_list)
