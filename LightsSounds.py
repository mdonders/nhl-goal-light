import os
import time
from pygame import mixer
import vlc

import config
import Receiver

if config.host_type == 'pi':
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    # Pin Definitons:
    GPIO.setmode(GPIO.BCM)  # Broadcom pin-numbering scheme
    GPIO.setup(config.relayPin, GPIO.OUT)
    print("PINS Configd")


def getLightsStatus():
    return os.path.isfile(config.filepath + '/python/nhlgoallight/status/lights')


def getSoundStatus():
    return os.path.isfile(config.filepath + '/python/nhlgoallight/status/sounds')


def goalHorn(status):
    if getSoundStatus() and config.host_type == 'pi':
        if status == "enable":
            mixer.init()
            # Receiver.set_power_state("ON")
            Receiver.set_sd_mode("ANALOG")
            # Receiver.send_telnet("SDANALOG")
            time.sleep(1)
            Receiver.set_volume(60)
            mixer.music.load(config.filepath + '/python/nhlgoallight/Devils2015GoalSong.mp3')
            # mixer.music.load(config.filepath + '/python/nhlgoallight/Devils2012GoalSong.mp3')
            mixer.music.play()

        elif status == "disable":
            Receiver.set_sd_mode("AUTO")
            mixer.music.stop()


def goalLights(status):
    if getLightsStatus() and config.host_type == 'pi':
        if status == "enable":
            GPIO.output(config.relayPin, GPIO.HIGH)
        elif status == "disable":
            GPIO.output(config.relayPin, GPIO.LOW)


def goalHornByTeam(status, team):
    if getSoundStatus() and config.host_type == 'pi':
        if status == "enable":
            mixer.init()
            # Receiver.set_power_state("ON")
            Receiver.set_sd_mode("ANALOG")
            # Receiver.send_telnet("SDANALOG")
            time.sleep(1)
            Receiver.set_volume(60)
            mixer.music.load(config.filepath + '/python/nhlgoallight/goalsongs/'+ team + '.mp3')
            # mixer.music.load(config.filepath + '/python/nhlgoallight/Devils2012GoalSong.mp3')
            mixer.music.play()

        elif status == "disable":
            Receiver.set_sd_mode("AUTO")
            mixer.music.stop()
