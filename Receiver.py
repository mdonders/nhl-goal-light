#!/usr/local/bin/python3

import config

import requests
import os
import telnetlib
from xml.etree import ElementTree

denon_ip = '192.168.1.242'
status_url = '/goform/formMainZone_MainZoneXml.xml'

# ------------------------------------------------------------------------------
# GETTERS & SETTERS - RECEIVER FUNCTIONS
# ------------------------------------------------------------------------------


def get_friendly_name():
	url = 'http://' + config.RECEIVER_IP_ADDRESS + status_url
	response = requests.get(url)
	root = ElementTree.fromstring(response.content)

	for friendly in root.iter('FriendlyName'):
		print("Friendly Name - ", friendly[0].text)


def get_power_state():
	url = 'http://' + denon_ip + status_url
	try:
		response = requests.get(url, timeout=1)
		root = ElementTree.fromstring(response.content)
		for value in root.iter('Power'):
			powerState = value[0].text
			# print("Power State - ", powerState)
	except requests.exceptions.RequestException as e:
		print("Network Exception - ", e)
		powerState = 'N/A'
	except:
		print('Uncaught Exception getting Power State')
		powerState = 'N/A'
	return powerState


def set_power_state(powerState):
	url = 'http://' + denon_ip + '/MainZone/index.put.asp?cmd0=PutZone_OnOff/' + powerState
	requests.get(url)


def get_mute_state():
	url = 'http://' + denon_ip + status_url
	response = requests.get(url)
	root = ElementTree.fromstring(response.content)
	for value in root.iter('Mute'):
		print("Mute State - ", value[0].text)


def get_input():
	url = 'http://' + denon_ip + status_url
	response = requests.get(url)
	root = ElementTree.fromstring(response.content)
	for value in root.iter('InputFuncSelect'):
		currentInput = value[0].text
		# print("Current Input - ", currentInput)
	return currentInput


def set_input(input):
	url = 'http://' + denon_ip + '/goform/formiPhoneAppDirect.xml?SI' + input
	requests.get(url)


def get_volume():
	url = 'http://' + denon_ip + status_url
	try:
		response = requests.get(url, timeout=1)
		root = ElementTree.fromstring(response.content)
		for value in root.iter('MasterVolume'):
			masterVolume = float(value[0].text) + 80
			# print("Master Volume - ", masterVolume)
	except requests.exceptions.RequestException as e:
		print("Network Exception - ", e)
		masterVolume = 'N/A'
	except:
		print('Uncaught Exception getting Master Volume')
		masterVolume = 'N/A'
	return masterVolume


def set_volume(volume):
	vol = volume - 80
	url = 'http://' + denon_ip + '/goform/formiPhoneAppVolume.xml?1+' + str(vol)
	requests.get(url)


def set_sd_mode(mode):
	sdMode = 'SD' + mode
	telnetCommmand = '{ echo "' + sdMode + '"; sleep 1; } | telnet ' + denon_ip
	os.system(telnetCommmand)


def send_telnet(command):
	tn = telnetlib.Telnet(denon_ip)
	tn.write(command.encode('ASCII') + b'\r')
	print(tn.read_until(b'\r', timeout=0.2).decode('UTF-8').strip())
	tn.close()


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# MAIN PROGRAM FLOW
# ------------------------------------------------------------------------------
# get_friendly_name()
# get_power_state()
# get_mute_state()
# get_input()
# get_volume()
# set_input('DVD')
# set_volume(44)
# set_power_state('OFF')
