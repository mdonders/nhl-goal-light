#!/usr/local/bin/python3

import time

# Custom Imports
import config
import Receiver
import LightsSounds


if config.host_type == 'pi':
	import RPi.GPIO as GPIO


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# MAIN PROGRAM FLOW
# ------------------------------------------------------------------------------

try:
	if config.host_type == 'pi':
		print(Receiver.get_input())
		print(Receiver.get_power_state())
		LightsSounds.goalHorn("enable")
		# Volume Changer
		Receiver.set_volume(30)
		LightsSounds.goalLights("enable")

		time.sleep(config.goal_time)
		LightsSounds.goalHorn("disable")
		LightsSounds.goalLights("disable")
		GPIO.cleanup()

	else:
		print(Receiver.get_input())
		print(Receiver.get_power_state())
		LightsSounds.goalHorn("enable")
		time.sleep(config.goal_time)
		LightsSounds.goalHorn("disable")


except KeyboardInterrupt:
	print('---------------------------------------')
	print('Keyboard Interrupt Received - Cleaning Up...')

	if config.host_type == 'pi':
		# Restore GPIO to default state
		LightsSounds.goalHorn("disable")
		LightsSounds.goalLights("disable")
		GPIO.cleanup()

	else:
		LightsSounds.goalHorn("disable")
