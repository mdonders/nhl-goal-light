#!/usr/local/bin/python3

import binascii
import time
import os
import sys

import logging  # for the following line
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)  # suppress IPV6 warning on startup
from scapy.all import *
from scapy.utils import *

# Custom Imports
import config
import LightsSounds

# it takes a minute for the scapy sniffing to initialize, so I print this to know when it's actually ready to go
print('Initialization done - checking local packets.')

# Define some Global Variables
count = 0
lasttime = 0

# Get script path to use for Status Variables & Relative location
script_path = os.path.dirname(os.path.realpath(sys.argv[0]))

def setButtonGoalStatus():
    filename = script_path + '/status/goalDevilsButton'
    open(filename, 'w').close()
    os.chmod(filename, 0o777)

def lights_and_sounds():
	LightsSounds.goalHorn('enable')
	LightsSounds.goalLights('enable')

	time.sleep(config.goal_time)
	LightsSounds.goalHorn('disable')
	LightsSounds.goalLights('disable')

def button_pressed(time):
    global lasttime
    diff = time - lasttime
    if (diff > 5):
        print("Cascade Dash Button Pressed! Last press (seconds ago) - ", int(diff))
        setButtonGoalStatus()
        lights_and_sounds()
    # else:
        # DEBUG
        # print("Button pressed too quickly - ignoring!")
    lasttime = time


def udp_packet(pkt):
    global count

    try:
        macaddr = binascii.hexlify(pkt.getlayer(BOOTP).chaddr)
        if config.dash_button in str(macaddr):
            button_pressed(time.time())

        else:
            print("CHADDR caught, but not the correct Dash button!")
    except AttributeError:
        pass
        # print("Attribute Error, probably not a dash button. Error - ", e)


print(sniff(prn=udp_packet, filter="udp", store=0))
