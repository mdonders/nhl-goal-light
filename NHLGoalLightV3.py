#!/usr/local/bin/python3

import os
import sys
import datetime
import dateutil.tz
import time
import requests
import json
from pygame import mixer

# Custom Imports
import config
import LightsSounds

# Config GPIO on Raspberry Pi if Running on Pi
# TODO: Use either of the following for hostname
# socket.gethostname()
# platform.node()
if config.host_type == 'pi':
	import RPi.GPIO as GPIO
	GPIO.setwarnings(False)
	# Pin Definitons:
	relayPin = 17  # Broadcom pin 17 (P1 pin 11)
	GPIO.setmode(GPIO.BCM)  # Broadcom pin-numbering scheme
	GPIO.setup(relayPin, GPIO.OUT)

# Get script path to use for Status Variables & Relative location
script_path = os.path.dirname(os.path.realpath(sys.argv[0]))
# print('[DEBUG] Script Path - ', script_path)

# DEBUG - Check Python Version on Start
# print('Python Version - ', sys.version_info)

# Variable to store file path (for working on OSX & Raspberry Pi)
# filepath = '/Users/mattdonders/Development'

# mixer.init()
# mixer.music.load('/Users/mattdonders/Development/python/nhlgoallight/Devils2015GoalSong.mp3')
# mixer.music.play()

# IFTTT Web Request (Testing)
ifttt = 'https://maker.ifttt.com/trigger/devils_goal_scored/with/key/dx3LQ6RuBSK3IfAgE126ze'

# Uncomment for Sharks (testing)
# team_name = "San Jose Sharks"
# team_id = 28

# Uncomment for Lightning (testing)
# team_name = "Tampa Bay Lightning"
# team_id = 14


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# RPi Hardware Functions
# ------------------------------------------------------------------------------

def getLightsStatus():
	return os.path.isfile(config.filepath + '/python/nhlgoallight/status/lights')


def getSoundStatus():
	return os.path.isfile(config.filepath + '/python/nhlgoallight/status/sounds')


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# NHL Score & Parsing APIs
# ------------------------------------------------------------------------------

def get_team(team):
	"""
	Passes team name to NHL API and returns team ID.
	:param team: Valid NHL team name.
	"""

	team_name = team.title()
	url = 'http://statsapi.web.nhl.com/api/v1/teams'
	team_list = requests.get(url)
	team_json = team_list.json()
	teams = team_json["teams"]

	for team in teams:
		if team["name"] == team_name:
			team_id = team["id"]
			# print("Team Name - " + team["name"] + " (", team_id) + ")"
	return team_id


def download_schedule():
	now = datetime.datetime.now()
	today = now.strftime('%Y-%m-%d')
	team_id = get_team(config.teamName)
	schedule_file = script_path + '/status/front_end_info_new'
	if not os.path.exists(schedule_file):
		open(schedule_file, 'w+').close()

	json_dump_file = script_path + '/status/front_end_json_dump'
	if not os.path.exists(json_dump_file):
		open(json_dump_file, 'w+').close()

	schedule_file_size = os.path.getsize(schedule_file)
	schedule_file_mod = datetime.datetime.fromtimestamp(os.path.getmtime(schedule_file)).strftime('%Y-%m-%d')

	if schedule_file_mod == today and schedule_file_size > 0:
		print(dateLogger(), "[MAIN] Schedule is Up to Date - Not Downloading!")
		return
	else:
		print(dateLogger(), "[MAIN] Schedule is Out of Date - Downloading Now!")

	url = 'https://statsapi.web.nhl.com/api/v1/schedule?startDate={:%Y-%m-%d}&endDate=2018-04-09&expand=schedule.teams,schedule.broadcasts&site=en_nhl&teamId={}'.format(now, team_id)
	try:
		schedule_request = requests.get(url)
		schedule_json = schedule_request.json()
		with open(json_dump_file, 'w') as outfile:
			json.dump(schedule_json, outfile, indent=4)

	except requests.exceptions.RequestException:    # This is the correct syntax
		print(json.dumps(schedule_json, indent=4, sort_keys=False))
		return

	schedule_info = {}
	# Format Game Date
	game_date = schedule_json['dates'][0]['games'][0]['gameDate']
	game_date = datetime.datetime.strptime(game_date, '%Y-%m-%dT%H:%M:%SZ')
	game_date_formatted = game_date.strftime('%b %d, %G')

	schedule_info['next_game_date'] = game_date_formatted
	schedule_info['is_game_today'] = True if schedule_info['next_game_date'] == today else False
	schedule_info['home_team'] = schedule_json['dates'][0]['games'][0]['teams']['home']['team']['name']
	schedule_info['away_team'] = schedule_json['dates'][0]['games'][0]['teams']['away']['team']['name']
	schedule_info['my_team_home'] = True if schedule_info['home_team'] == config.teamName.title() else False

	# Format Game Time
	localtz = dateutil.tz.tzlocal()
	localoffset = localtz.utcoffset(datetime.datetime.now(localtz))

	game_date = schedule_json['dates'][0]['games'][0]['gameDate']
	game_date = datetime.datetime.strptime(game_date, '%Y-%m-%dT%H:%M:%SZ')
	game_date_local = game_date + localoffset
	game_time_local_ampm = game_date_local.strftime('%I:%M %p')
	schedule_info['game_time'] = game_time_local_ampm

	## Sometimes broadcast information is not yet available
	try:
		broadcasts = schedule_json['dates'][0]['games'][0]['broadcasts']
		for broadcast in broadcasts:
			if schedule_info['my_team_home'] and broadcast['type'] == 'home':
				schedule_info['broadcast_channel'] = broadcast['name']
			elif not schedule_info['my_team_home'] and broadcast['type'] == 'away':
				schedule_info['broadcast_channel'] = broadcast['name']
		try:
			schedule_info['broadcast_channel_no'] = config.channels[schedule_info['broadcast_channel']]
		except KeyError:
			schedule_info['broadcast_channel_no'] = 'N/A'
	except KeyError:
		schedule_info['broadcast_channel_no'] = 'N/A'

	schedule_info['venue'] = schedule_json['dates'][0]['games'][0]['teams']['home']['team']['venue']['name']

	# Write schedule info to our "new" front-end file
	with open(schedule_file, 'w') as outfile:
		json.dump(schedule_info, outfile, indent=4)

	return schedule_info


def is_season():
	# If Month is July, August or September
	# Hockey is not in Season (Sleep Longer)
	now = datetime.datetime.now()
	if now.month in (7, 8):
		return False
	else:
		return True


def is_game_today(id):
	now = datetime.datetime.now()
	url = 'http://statsapi.web.nhl.com/api/v1/schedule?teamId={}&date={:%Y-%m-%d}'.format(id, now)
	try:
		games_today = requests.get(url)
		games_json = games_today.json()
		games_total = games_json["totalItems"]
		# print json.dumps(games_today.json(), indent=4, sort_keys=False)
		# print("Total Items - ", games_today.json()["totalItems"])
	except requests.exceptions.RequestException:
		return

	if games_total == 1:
		return True
	else:
		return False


def get_game_info(id):
	now = datetime.datetime.now()
	team_name = config.teamName.title()
	url = 'http://statsapi.web.nhl.com/api/v1/schedule?teamId={}&date={:%Y-%m-%d}'.format(id, now)

	try:
		score_request = requests.get(url)
		json_request = score_request.json()

	except requests.exceptions.RequestException:    # This is the correct syntax
		print(json.dumps(json_request, indent=4, sort_keys=False))
		return

	dates = json_request["dates"][0]["games"][0]
	game_date = dates["gameDate"]
	game_date = datetime.datetime.strptime(game_date, '%Y-%m-%dT%H:%M:%SZ')
	# TODO - Convert to local time
	game_status = dates["status"]["abstractGameState"]
	game_time_countdown = game_date - now
	game_time_countdown = game_time_countdown.total_seconds()

	localtz = dateutil.tz.tzlocal()
	localoffset = localtz.utcoffset(datetime.datetime.now(localtz))

	game_date_local = game_date + localoffset
	game_time_local = game_date_local.strftime('%H:%M:%S')
	game_time_local_ampm = game_date_local.strftime('%I:%M %p')
	game_time_countdown_local = (game_date_local - now).total_seconds()
	# game_time_countdown = game_time_countdown - localoffset_seconds

	# print("[DEBUG] Game Status - " + game_status
	# print("[DEBUG] Game Date (UTC) - ", game_date)
	print(dateLogger(), "[MAIN] Game Date (Local) - ", game_date_local)
	print(dateLogger(), "[MAIN] Game Time (Local) - ", game_time_local)
	if game_time_countdown_local < 0:
		print(dateLogger(), "[MAIN] Game Countdown (s) - N/A")
	else:
		print(dateLogger(), "[MAIN] Game Countdown (s) - ", game_time_countdown_local)

	teams = json_request["dates"][0]["games"][0]["teams"]
	home_team = teams["home"]["team"]["name"]
	away_team = teams["away"]["team"]["name"]

	if home_team == team_name:
		myteam_location = "home"
		other_location = "away"
		my_team = home_team
		other_team = away_team
	else:
		myteam_location = "away"
		other_location = "home"
		my_team = away_team
		other_team = home_team

	myteam_score = teams[myteam_location]["score"]
	otherteam_score = teams[other_location]["score"]

	# print("[DEBUG] Current Score of ", my_team, " - ", myteam_score)
	# print("[DEBUG] Current Score of ", other_team, " - ", otherteam_score)

	game_info_dict = {}
	game_info_dict['countdown'] = game_time_countdown_local
	game_info_dict['game_time_local'] = game_time_local
	game_info_dict['game_time_local_ampm'] = game_time_local_ampm
	game_info_dict['status'] = game_status
	game_info_dict['location'] = myteam_location
	game_info_dict['other_team'] = other_team
	game_info_dict['myteam_score'] = myteam_score
	game_info_dict['otherteam_score'] = otherteam_score

	game_info = []
	game_info.append(game_time_countdown_local)		# 0 - Local Countdown in Seconds
	game_info.append(game_status)					# 1 - Game Status (Preview, Live, etc)
	game_info.append(myteam_score)					# 2 - My Team Retrieved Score
	game_info.append(otherteam_score)				# 3 - Other Team Retrieved Score
	game_info.append(other_team)					# 4 - Other Team Name
	game_info.append(myteam_location)				# 5 - My Team Location
	game_info.append(game_time_local)				# 6 - Local Game Time
	game_info.append(game_time_local_ampm)			# 7 - Local Game Time (AM/PM)
	return game_info


def front_end_gameinfo(game_info):
	front_end_file = script_path + '/status/front_end_info'
	game_status = game_info[1]

	if game_status == 'Preview':
		data = {'status': game_info[1], 'game_time': game_info[7], 'opponent': game_info[4], 'location': game_info[5]}
	elif game_status == 'None':
		data = {'status': 'None'}
	else:
		data = {'status': 'N/A'}

	with open(front_end_file, 'w') as outfile:
		json.dump(data, outfile, indent=4)


def getManualGoalStatus():
	return os.path.isfile(config.filepath + '/python/nhlgoallight/status/goalDevils')


def getButtonGoalStatus():
	return os.path.isfile(config.filepath + '/python/nhlgoallight/status/goalDevilsButton')


def wait(amount):
	now = datetime.datetime.now()
	today = datetime.datetime.today()

	if amount == "day":
		delta = datetime.timedelta(days=1)

	elif amount == "season":
		if now.month is 8:
			delta = datetime.timedelta(days=31)
		else:
			delta = datetime.timedelta(days=30)

	next_check = today + delta
	next_check = next_check.replace(hour=0, minute=0)
	print("[DEBUG] Next Time to Check - ", next_check)
	sleep_time = next_check - now
	sleep_time = sleep_time.total_seconds()
	print("[DEBUG] Check in Seconds - ", sleep_time)
	time.sleep(sleep_time)


# print('---------------------------------------'
# print('          TESTING FUNCTIONS            '
# print('---------------------------------------'

# print("Hockey Season - ", is_season())
# print("Team Name     - " + team_name
# team_id = get_team(team_name)
# print("Team ID       - ", team_id)
# print("Game Today    - ", is_game_today(team_id))
# scores = get_game_info(team_id)
# print("My Team Score - ", scores[0])
# print("Other Score   - ", scores[1])


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# MAIN PROGRAM FLOW
# ------------------------------------------------------------------------------

def dateFormatter(dateFormat):
	now = datetime.datetime.now()
	# nowstring = now.strftime('%H:%M:%S')
	nowstring = now.strftime(dateFormat)
	return nowstring


def dateLogger():
	logstring = '[' + dateFormatter('%H:%M:%S') + ']'
	return logstring


def pidManager():
	pid = str(os.getpid())
	global pidfile
	pidfile = config.filepath + '/python/nhlgoallight/status/status.pid'

	if os.path.isfile(pidfile):
		print(pidfile, 'already exists, exiting...')
		sys.exit()
	print(dateLogger(), '[DEBUG] Creating Status file for Process ID - ', pid)
	pid_file = open(pidfile, 'w')
	pid_file.write(pid)
	pid_file.close()


def reset_variables():
	# Reset Variables
	global myteam_current_score
	global myteam_new_score
	global otherteam_current_score
	global otherteam_new_score
	global gameday
	global season

	myteam_current_score = 0
	myteam_new_score = 0
	otherteam_current_score = 0
	otherteam_new_score = 0
	gameday = False
	season = False

	# Print Variable Status (For DEBUG Purposes)
	print(dateLogger(), '[VAR IN FUNC] RESETTING VARIABLES TO DEFAULT VALUES')

	# print('[VAR IN FUNC] Global "My Team Current Score" - ', myteam_current_score)
	# print('[VAR IN FUNC] Global "My Team New Score" - ', myteam_new_score)
	# print('[VAR IN FUNC] Global "Other Team Current Score" - ', otherteam_current_score)
	# print('[VAR IN FUNC] Global "Other Team New Score" - ', otherteam_new_score)
	# print('[VAR IN FUNC] Global "Season" - ', season)
	# print('[VAR IN FUNC] Global "Gameday" - ', gameday)


try:
	# Set PID File (for Script Status in Front-End)
	pidManager()

	# Initialize Flow by Resetting All Variables
	reset_variables()

	localtz = dateutil.tz.tzlocal()
	team_id = get_team(config.teamName)
	team_name = config.teamName.title()
	print(dateLogger(), '[GLOBAL] Team Name = ', config.teamName)
	print(dateLogger(), '[GLOBAL] Team ID = ', team_id)

	print('')
	print('---------------------------------------')
	print('            MAIN CODE FLOW             ')
	print('---------------------------------------')

	while(1):
		if not season or not gameday:
			print(dateLogger(), '[MAIN] Today = ' + dateFormatter('%A, %B %d %Y'))
			# print(dateLogger(), '[MAIN] Season OR Gameday is False, Check Status...')
			season = is_season()
			gameday = is_game_today(team_id)

		if season:
			# If we are in season, download schedule
			download_schedule()
			print(dateLogger(), "[MAIN] Hockey Season = Yes!")

			if gameday:
				print(dateLogger(), "[MAIN] Game Today = Yes!")

				game_info = get_game_info(team_id)
				game_info_status = game_info[1]
				print('')
				print(dateLogger(), '[MAIN] Game Status (In Loop) - ', game_info_status)

				if game_info_status == "Preview":
					countdown = game_info[0]
					if countdown < 0:
						print(dateLogger(), '[MAIN] Preview Past Game Time - Sleep for 60 seconds.')
						time.sleep(60)
					else:
						# If there is a game today, this function kicks off at midnight
						# Load a file in JSON Format for Front-End to Read
						front_end_gameinfo(game_info)

						print(dateLogger(), '[MAIN] Preview - Opponent - ' + game_info[4])
						print(dateLogger(), '[MAIN] Preview - Sleep for ', countdown, ' seconds')
						time.sleep(countdown)

				elif game_info_status == "Live":
					now = datetime.datetime.now()
					nowstring = now.strftime('%H:%M:%S')

					team_name = config.teamName.title()
					myteam_new_score = game_info[2]
					otherteam_new_score = game_info[3]
					other_team = game_info[4]

					# Logic to Check if Script Started in Middle of Game
					myteam_score_diff = myteam_new_score - myteam_current_score
					otherteam_score_diff = otherteam_new_score - otherteam_current_score

					if myteam_score_diff > 1:
						myteam_current_score = myteam_new_score

					if otherteam_score_diff > 1:
						otherteam_current_score = otherteam_new_score

					print('---------------------------------------')
					print(dateLogger(), '[MAIN] ', team_name, ' - Current Score - ', myteam_current_score)
					print(dateLogger(), '[MAIN] ', team_name, ' - New Score - ', myteam_new_score)
					print(dateLogger(), '[MAIN] ', other_team, ' - Current Score - ', otherteam_current_score)
					print(dateLogger(), '[MAIN] ', other_team, ' - New Score - ', otherteam_new_score)
					print('---------------------------------------')

					# Chcek for
					# Check for "Manually Set Goal Score"
					if getButtonGoalStatus():
						# Remove Manual Goal File
						os.remove(config.filepath + '/python/nhlgoallight/status/goalDevilsButton')

						print(dateLogger(), '[MAIN] Goal Trigger by Dash Button')
						print(dateLogger(), '[GOAL] MY TEAM - BUTTON GOAL TRIGGERED')
						print(dateLogger(), '[GOAL] MY TEAM - Goal by the ' + team_name)
						print(dateLogger(), '[GOAL] Time of the Goal - ' + nowstring)
						myteam_current_score = myteam_current_score + 1

					elif getManualGoalStatus():
						# Remove Manual Goal File
						os.remove(config.filepath + '/python/nhlgoallight/status/goalDevils')

						print(dateLogger(), '[MAIN] Goal Trigger by Web Button')
						print(dateLogger(), '[GOAL] MY TEAM - MANUAL GOAL TRIGGERED')
						print(dateLogger(), '[GOAL] MY TEAM - Goal by the ' + team_name)
						print(dateLogger(), '[GOAL] Time of the Goal - ' + nowstring)
						myteam_current_score = myteam_current_score + 1

						if config.host_type == 'pi':
							LightsSounds.goalHorn('enable')
							LightsSounds.goalLights('enable')

							time.sleep(config.goal_time)
							LightsSounds.goalHorn('disable')
							LightsSounds.goalLights('disable')

					# Logic to Check if Score Has Changed
					# This shouldn't trigger the score twice since we updated the current score by 1
					elif myteam_new_score > myteam_current_score:
						print(dateLogger(), '[GOAL] MY TEAM - Goal by the ' + team_name)
						print(dateLogger(), '[GOAL] Time of the Goal - ' + nowstring)
						myteam_current_score = myteam_new_score

						if config.host_type == 'pi':
							LightsSounds.goalHorn('enable')
							LightsSounds.goalLights('enable')

							time.sleep(config.goal_time)
							LightsSounds.goalHorn('disable')
							LightsSounds.goalLights('disable')

						# requests.get(ifttt)

					elif otherteam_new_score > otherteam_current_score:
						print('[GOAL] OTHER TEAM - Goal by the ' + other_team)
						print('[GOAL] Time of the Goal - ' + nowstring)
						otherteam_current_score = otherteam_new_score

					## Update score in Front-End File
					front_end_file = script_path + '/status/front_end_info_new'
					with open(front_end_file) as data_file:
						game_info_new = json.load(data_file)

					game_info_new['status'] = 'Live'
					game_info_new['my_team_score'] = myteam_current_score
					game_info_new['other_team_score'] = otherteam_current_score

					with open(front_end_file, 'w') as outfile:
						json.dump(game_info_new, outfile, indent=4)

					time.sleep(5)

				elif game_info_status == "Final":
					# Update Front-End File for End of Game
					front_end_file = script_path + '/status/front_end_info_new'
					with open(front_end_file) as data_file:
						game_info_new = json.load(data_file)

					game_info_new['status'] = 'Final'
					game_info_new['my_team_score'] = myteam_current_score
					game_info_new['other_team_score'] = otherteam_current_score

					with open(front_end_file, 'w') as outfile:
						json.dump(game_info_new, outfile, indent=4)
					# Reset All Variables (To Reset Loop)

					reset_variables()
					wait("day")

			else:
				game_info = []
				game_info.append(0)								# 0 - Just to Pad the Array
				game_info.append('None')						# 1 - No Game Today
				front_end_gameinfo(game_info)

				print(dateLogger(), '[DEBUG] Game Today = No :(')
				wait("day")

		else:
			print(dateLogger(), '[DEBUG] Hockey Season = No :(')
			wait("season")

		print('---------------------------------------')

except KeyboardInterrupt:
	print('')
	print('---------------------------------------')
	print(dateLogger(), '[MAIN] Keyboard Interrupt Received - Cleaning Up...')

	if config.host_type == 'pi':
		# Restore GPIO to default state
		GPIO.cleanup()

	os.remove(pidfile)
