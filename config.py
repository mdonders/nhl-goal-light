filepath = '/Users/mattdonders/Development'
host_type = 'osx'
relayPin = 17

# Receiver Configuration
RECEIVER_IP_ADDRESS = '192.168.1.242'

# NHL Team Name to Check Scores
teamName = "New Jersey Devils"

# Length of Time Goal Horn & Lights Should Stay On
goal_time = 45

basicauth_user = 'admin'
basicauth_pass = 'letsgodevils'

post_lights = 'LightsAdmin123!'
post_sounds = 'SoundsAdmin123!'
post_test = 'Testing123!'
post_score = '1234'

# Channel Lookup
channels = {'MSG': '578', 'MSG2': '579', 'MSG+': '580', 'NBCSN': '590'}

# Dash Button MAC Address
dash_button_mac = '1a:a6:f7:b7:07:e2'
dash_button = '1aa6f7b707e2'
