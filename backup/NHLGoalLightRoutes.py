import os
from datetime import date
import time
from pygame import mixer

import config
import Receiver
import LightsSounds

# We are importing 'Flask' to create an application,
# 'render_template' to render the given template as the response

from flask import Flask, render_template, jsonify, request, Response
from functools import wraps

# Create a flask app and set a random secret key
app = Flask(__name__)
app.secret_key = os.urandom(24)

# Secure the flask app with basic authentication
app.config['BASIC_AUTH_USERNAME'] = config.basicauth_user
app.config['BASIC_AUTH_PASSWORD'] = config.basicauth_pass


# Define our authentication & response functions
def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    correct_username = app.config['BASIC_AUTH_USERNAME']
    correct_password = app.config['BASIC_AUTH_PASSWORD']
    return username == correct_username and password == correct_password


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response('Could not verify your access level for that URL.\n'
                    'You have to login with proper credentials', 401,
                    {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated


# ------------------------------------------------------------------------------
# GETTERS / SETTERS FOR LIGHTS AND SOUND CONTROLS
# ------------------------------------------------------------------------------

def getNextGame():
    today = date.today()
    future = date(2016, 10, 13)
    next_game_info = []
    next_game_info.append((future - today).days)            # 0 - Countdown in Days
    next_game_info.append(future)                           # 1 - Next Game Date
    next_game_info.append("@ Panthers")                     # 2 - Team Being Played
    return next_game_info


def getLightsStatus():
    return os.path.isfile(config.filepath + '/python/nhlgoallight/status/lights')


def setLightStatus(status):
    filename = config.filepath + '/python/nhlgoallight/status/lights'
    if (status == "enable"):
        open(filename, 'w').close()
    else:
        if os.path.exists(filename):
            os.remove(filename)


def getScriptStatus():
    filename = config.filepath + '/python/nhlgoallight/status/status.pid'
    if os.path.isfile(filename):
        pidfile = open(filename, 'r')
        pid = pidfile.readline()
        pidfile.close()

        try:
            os.kill(int(pid), 0)
        except OSError:
            return False
        else:
            return True
    else:
        return False


def getSoundStatus():
    return os.path.isfile(config.filepath + '/python/nhlgoallight/status/sounds')


def setSoundStatus(status):
    filename = config.filepath + '/python/nhlgoallight/status/sounds'
    if (status == "enable"):
        open(filename, 'w').close()
    else:
        if os.path.exists(filename):
            os.remove(filename)


def setSoundTestStatus(status):
    if status == "enable":
        mixer.init()
        # Receiver.set_power_state("ON")
        Receiver.set_sd_mode("ANALOG")
        # Receiver.send_telnet("SDANALOG")
        time.sleep(1)
        Receiver.set_volume(60)
        mixer.music.load(config.filepath + '/python/nhlgoallight/Devils2015GoalSong.mp3')
        mixer.music.play()

    else:
        Receiver.set_sd_mode("AUTO")
        mixer.music.stop()


def setManualGoalDevils():
    filename = config.filepath + '/python/nhlgoallight/status/goalDevils'
    open(filename, 'w').close()


# / route and its view. This renders the index.html template
@app.route('/')
@requires_auth
def index():
    print('Script Status - ', getScriptStatus())
    print('Lights Status - ', getLightsStatus())
    print('Sound Status  - ', getSoundStatus())
    print('Next Game - ', getNextGame())
    print('Authorized    - True')
    return render_template('index.html',
                           vReceiverPower=Receiver.get_power_state(), vReceiverVolume=Receiver.get_volume(), vNextGame=getNextGame(),
                           vScriptStatus=getScriptStatus(), vLightStatus=getLightsStatus(), vSoundStatus=getSoundStatus())


@app.route('/admin')
@requires_auth
def admin():
    print('Receiver Power - ', Receiver.get_power_state())
    print('Receiver Volume - ', Receiver.get_volume())
    print('Script Status - ', getScriptStatus())
    print('Lights Status - ', getLightsStatus())
    print('Sound Status  - ', getSoundStatus())
    print('Authorized    - True')
    return render_template('admin.html',
                           vReceiverPower=Receiver.get_power_state(), vReceiverVolume=Receiver.get_volume(),
                           vScriptStatus=getScriptStatus(), vLightStatus=getLightsStatus(), vSoundStatus=getSoundStatus())


@app.route('/debug')
@requires_auth
def debug():
    return render_template('debug.html')


# ------------------------------------------------------------------------------
# ROUTES FOR CHANGING LIGHTS & SOUNDS STATUS
# ------------------------------------------------------------------------------


@app.route('/lights/<status>', methods=['POST'])
def changeLightStatus(status):
    print('Lights Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_lights:
        print('Passowrd verified - changing Light Status!')
        print('Lights Status Request - ', status)
        setLightStatus(status)
        return jsonify(newStatus=status)
    else:
        print('Incorrect Password - not changing Light Status!')
        return jsonify(newStatus=getLightsStatus())


@app.route('/lights/test/<status>', methods=['POST'])
def changeLightTestStatus(status):
    print('Lights Test Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_lights:
        print('Passowrd verified - changing Light Test Status!')
        print('Lights Test Status Request - ', status)
        LightsSounds.goalLights(status)
        return jsonify(newStatus=status)
    else:
        print('Incorrect Password - not changing Light Status!')
        return jsonify(newStatus=getLightsStatus())


@app.route('/sounds/<status>', methods=['POST'])
def changeSoundStatus(status):
    print('Sounds Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_sounds:
        print('Passowrd verified - changing Sound Status!')
        print('Sounds Status Request - ', status)
        setSoundStatus(status)
        return jsonify(newStatus=status)
    else:
        print('Incorrect Password - not changing Sounds Status!')
        return jsonify(newStatus=getSoundStatus())


# ------------------------------------------------------------------------------
# ROUTES FOR TESTING LIGHTS & SOUNDS
# ------------------------------------------------------------------------------


@app.route('/tests/lights', methods=['POST'])
def performTestsLights():
    print('Full Test Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_test:
        print('Passowrd verified - Testing Lights!')

        LightsSounds.goalLights("enable")
        time.sleep(config.goal_time)
        LightsSounds.goalLights("disable")
        # GPIO.cleanup()

        return True
    else:
        print('Incorrect Password - NOT Testing Lights & Sounds!')
        return False


@app.route('/tests/sounds', methods=['POST'])
def performTestsSounds():
    print('Sound Test Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_test:
        print('Passowrd verified - Testing Sounds!')

        if(Receiver.get_power_state() == 'STANDBY'):
            powerFlipped = True
            Receiver.set_power_state('ON')
            time.sleep(1)

        LightsSounds.goalHorn("enable")
        Receiver.set_volume(30)
        time.sleep(config.goal_time)
        LightsSounds.goalHorn("disable")
        # GPIO.cleanup()

        if powerFlipped is True:
            Receiver.set_power_state('OFF')

        return True
    else:
        print('Incorrect Password - NOT Testing Lights & Sounds!')
        return False


@app.route('/tests/full', methods=['POST'])
def performTestsFull():
    print('Full Test Password POSTed - ', request.form['password'])
    if request.form['password'] == config.post_test:
        print('Passowrd verified - Testing Lights & Sounds!')

        if Receiver.get_power_state() == 'STANDBY':
            powerFlipped = True
            print('Receiver was off - powerFlipped is', powerFlipped)
            Receiver.set_power_state('ON')
            time.sleep(2)

        LightsSounds.goalHorn('enable')
        LightsSounds.goalLights('enable')

        time.sleep(config.goal_time)
        LightsSounds.goalHorn('disable')
        LightsSounds.goalLights('disable')
        # GPIO.cleanup()

        if powerFlipped is True:
            Receiver.set_power_state('OFF')
            print('Receiver was off - turning it back off now...')

        return jsonify(success=True), 200

    else:
        print('Incorrect Password - NOT Testing Lights & Sounds!')
        return sonify(success=False), 401


# ------------------------------------------------------------------------------
# ROUTES FOR SETTING DEVILS GOAL STATUS
# ------------------------------------------------------------------------------


@app.route('/devilsscored', methods=['POST'])
def devils_scored():
    if request.form['password'] == config.post_score:
        print('Passowrd verified - setting Devils Scored!')
        setManualGoalDevils()
        return jsonify(scored=True)
    else:
        print('Incorrect Password - not setting Devils Scored!')
        return jsonify(scored=False)


"""
# Illustration of a single view handling multiple routes
@app.route('/route1/')
@app.route('/route2/')
@app.route('/lengthy/route3/')
def multiple_routes():
    message = "A single view can handle multiple routes"
    return render_template('page.html', message=message)


# Illustration of the difference between route with and without a trailing slash
@app.route('/no-slash')
@app.route('/slash/')
def trailing_slash():
    message = "If the route URL has a trailing slash, there will be a redirect from the same URL without / to the one with /. If there is no trailing slash, this view will not handle the URL with trailing /."
    return render_template('page.html', message=message)


# Illustration of capturing variable data from the URL
@app.route('/message/<msg>/')
def capture_value(msg):
    message = "Variable parts in the route can be specified with angular brackets and a variable name. They will be captured using the specified name. "
    message += "Value captured in this view is the message - %s" % (msg)
    return render_template('page.html', message=message)


# Illustration of capturing variable data from the URL and converting it to a type
@app.route('/users/<int:userid>/')
def capture_value_int(userid):
    message = "It is possible to capture variable parts and convert to various type. Here we are using int. Others are string (default), path, float"
    return render_template('page.html', message=message)


# Illustration of specifying which HTTP methods a view handles for a route
@app.route('/method/get/', methods=['GET'])
def handle_get_only():
    message = "It is possible to specify the methods a view will handle. This view handles only GET. Defaults are GET, HEAD and OPTIONS"
    return render_template('page.html', message=message)


# Illustration of default values for the value captured from the URL
@app.route('/page/', defaults = {'page':1})
@app.route('/page/<int:page>/')
def show_page(page):
    message = "This is page number %d. If no page number is specified, the view defaults to page 1" % page
    return render_template('page.html', message=message)
"""

if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("9999")
    )
