#!/usr/local/bin/python3

import os
import sys
import datetime
import dateutil.tz
import time
import requests
import json
from pygame import mixer

# Custom Imports
import config
import Receiver
import LightsSounds

# Config GPIO on Raspberry Pi if Running on Pi
# TODO: Use either of the following for hostname
# socket.gethostname()
# platform.node()
if config.host_type == 'pi':
	import RPi.GPIO as GPIO
	GPIO.setwarnings(False)
	# Pin Definitons:
	relayPin = 17  # Broadcom pin 17 (P1 pin 11)
	GPIO.setmode(GPIO.BCM)  # Broadcom pin-numbering scheme
	GPIO.setup(relayPin, GPIO.OUT)

# Get script path to use for Status Variables & Relative location
script_path = os.path.dirname(os.path.realpath(sys.argv[0]))
# print('[DEBUG] Script Path - ', script_path) 

# DEBUG - Check Python Version on Start
# print('Python Version - ', sys.version_info)

# Variable to store file path (for working on OSX & Raspberry Pi)
# filepath = '/Users/mattdonders/Development'

# mixer.init()
# mixer.music.load('/Users/mattdonders/Development/python/nhlgoallight/Devils2015GoalSong.mp3')
# mixer.music.play()

# IFTTT Web Request (Testing)
ifttt = 'https://maker.ifttt.com/trigger/devils_goal_scored/with/key/dx3LQ6RuBSK3IfAgE126ze'

# Uncomment for Sharks (testing)
# team_name = "San Jose Sharks"
# team_id = 28

# Uncomment for Lightning (testing)
# team_name = "Tampa Bay Lightning"
# team_id = 14


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# RPi Hardware Functions
# ------------------------------------------------------------------------------

def getLightsStatus():
	return os.path.isfile(config.filepath + '/python/nhlgoallight/status/lights')


def beacon_lights(status):
	if getLightsStatus:
		if status == on:
			GPIO.output(relayPin, GPIO.HIGH)
		elif status == off:
			GPIO.output(relayPin, GPIO.LOW)


def getSoundStatus():
	return os.path.isfile(config.filepath + '/python/nhlgoallight/status/sounds')


def goal_horn(status):
	if getSoundStatus:
		if status == on:
			Receiver.set_input('DVD')
			mixer.init()
			mixer.music.load(config.filepath + '/python/nhlgoallight/Devils2015GoalSong.mp3')
			mixer.music.play()

		elif status == off:
			GReceiver.set_input('CBL/SAT')
			# Not sure if STOP or QUIT (try both for testing)
			mixer.music.quit()
			# mixer.music.stop()


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# NHL Score & Parsing APIs
# ------------------------------------------------------------------------------

def get_team(team):
	"""
	Passes team name to NHL API and returns team ID.
	:param team: Valid NHL team name.
	"""

	team_name = team.title()
	url = 'http://statsapi.web.nhl.com/api/v1/teams'
	team_list = requests.get(url)
	team_json = team_list.json()
	teams = team_json["teams"]

	for team in teams:
		if team["name"] == team_name:
			team_id = team["id"]
			# print("Team Name - " + team["name"] + " (", team_id) + ")"

	return team_id


def is_season():
	# If Month is July, August or September
	# Hockey is not in Season (Sleep Longer)
	now = datetime.datetime.now()
	if now.month in (7, 8):
		return False
	else:
		return True


def is_game_today(id):
	now = datetime.datetime.now()
	url = 'http://statsapi.web.nhl.com/api/v1/schedule?teamId={}&date={:%Y-%m-%d}'.format(id, now)
	try:
		games_today = requests.get(url)
		games_json = games_today.json()
		games_total = games_json["totalItems"]
		# print json.dumps(games_today.json(), indent=4, sort_keys=False)
		# print("Total Items - ", games_today.json()["totalItems"])
	except requests.exceptions.RequestException:
		return

	if games_total == 1:
		return True
	else:
		return False


def get_game_info(id):
	now = datetime.datetime.now()
	team_name = config.teamName.title()
	url = 'http://statsapi.web.nhl.com/api/v1/schedule?teamId={}&date={:%Y-%m-%d}'.format(id, now)

	try:
		score_request = requests.get(url)
		json_request = score_request.json()

	except requests.exceptions.RequestException:    # This is the correct syntax
		print(json.dumps(json_request, indent=4, sort_keys=False))
		return

	dates = json_request["dates"][0]["games"][0]
	game_date = dates["gameDate"]
	game_date = datetime.datetime.strptime(game_date, '%Y-%m-%dT%H:%M:%SZ')
	# TODO - Convert to local time
	game_status = dates["status"]["abstractGameState"]
	game_time_countdown = game_date - now
	game_time_countdown = game_time_countdown.total_seconds()

	localtz = dateutil.tz.tzlocal()
	localoffset = localtz.utcoffset(datetime.datetime.now(localtz))

	game_date_local = game_date + localoffset
	game_time_local = game_date_local.strftime('%H:%M:%S')
	game_time_countdown_local = (game_date_local - now).total_seconds()
	# game_time_countdown = game_time_countdown - localoffset_seconds

	# print("[DEBUG] Game Status - " + game_status
	# print("[DEBUG] Game Date (UTC) - ", game_date)
	print(dateLogger(), "[MAIN] Game Date (Local) - ", game_date_local)
	print(dateLogger(), "[MAIN] Game Time (Local) - ", game_time_local)
	if game_time_countdown_local < 0:
		print(dateLogger(), "[MAIN] Game Countdown (s) - N/A")
	else:
		print(dateLogger(), "[MAIN] Game Countdown (s) - ", game_time_countdown_local)

	teams = json_request["dates"][0]["games"][0]["teams"]
	home_team = teams["home"]["team"]["name"]
	away_team = teams["away"]["team"]["name"]

	if home_team == team_name:
		myteam_location = "home"
		other_location = "away"
		my_team = home_team
		other_team = away_team
	else:
		myteam_location = "away"
		other_location = "home"
		my_team = away_team
		other_team = home_team

	myteam_score = teams[myteam_location]["score"]
	otherteam_score = teams[other_location]["score"]

	# print("[DEBUG] Current Score of ", my_team, " - ", myteam_score)
	# print("[DEBUG] Current Score of ", other_team, " - ", otherteam_score)

	game_info = []
	game_info.append(game_time_countdown_local)		# 0 - Local Countdown in Seconds
	game_info.append(game_status)					# 1 - Game Status (Preview, Live, etc)
	game_info.append(myteam_score)					# 2 - My Team Retrieved Score
	game_info.append(otherteam_score)				# 3 - Other Team Retrieved Score
	game_info.append(other_team)					# 4 - Other Team Name
	game_info.append(myteam_location)				# 5 - My Team Location
	game_info.append(game_time_local)				# 6 - Local Game Time
	return game_info


def front_end_gameinfo(game_info):
	front_end_file = script_path + '/status/front_end_info'
	data = {'status': game_info[1], 'game_time': game_info[6], 'opponent': game_info[4], 'location': game_info[5]}
	with open(front_end_file, 'w') as outfile:
		json.dump(data, outfile, indent=4)


def getManualGoalStatus():
	return os.path.isfile(config.filepath + '/python/nhlgoallight/status/goalDevils')


def wait(amount):
	now = datetime.datetime.now()
	today = datetime.datetime.today()

	if amount == "day":
		delta = datetime.timedelta(days=1)

	elif amount == "season":
		if now.month is 8:
			delta = datetime.timedelta(days=31)
		else:
			delta = datetime.timedelta(days=30)

	next_check = today + delta
	next_check = next_check.replace(hour=0, minute=0)
	print("[DEBUG] Next Time to Check - ", next_check)
	sleep_time = next_check - now
	sleep_time = sleep_time.total_seconds()
	print("[DEBUG] Check in Seconds - ", sleep_time)
	time.sleep(sleep_time)


# print('---------------------------------------'
# print('          TESTING FUNCTIONS            '
# print('---------------------------------------'

# print("Hockey Season - ", is_season())
# print("Team Name     - " + team_name
# team_id = get_team(team_name)
# print("Team ID       - ", team_id)
# print("Game Today    - ", is_game_today(team_id))
# scores = get_game_info(team_id)
# print("My Team Score - ", scores[0])
# print("Other Score   - ", scores[1])


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# MAIN PROGRAM FLOW
# ------------------------------------------------------------------------------

def dateFormatter(dateFormat):
	now = datetime.datetime.now()
	# nowstring = now.strftime('%H:%M:%S')
	nowstring = now.strftime(dateFormat)
	return nowstring


def dateLogger():
	logstring = '[' + dateFormatter('%H:%M:%S') + ']'
	return logstring

def pidManager():
	pid = str(os.getpid())
	global pidfile 
	pidfile = config.filepath + '/python/nhlgoallight/status/status.pid'

	if os.path.isfile(pidfile):
		print(pidfile, 'already exists, exiting...')
		sys.exit()
	print(dateLogger(), '[DEBUG] Creating Status file for Process ID - ', pid)
	pid_file = open(pidfile, 'w')
	pid_file.write(pid)
	pid_file.close()


def reset_variables():
	# Reset Variables
	global myteam_current_score
	global myteam_new_score
	global otherteam_current_score
	global otherteam_new_score
	global gameday
	global season

	myteam_current_score = 0
	myteam_new_score = 0
	otherteam_current_score = 0
	otherteam_new_score = 0
	gameday = False
	season = False

	# Print Variable Status (For DEBUG Purposes)
	print(dateLogger(), '[VAR IN FUNC] RESETTING VARIABLES TO DEFAULT VALUES')

	# print('[VAR IN FUNC] Global "My Team Current Score" - ', myteam_current_score)
	# print('[VAR IN FUNC] Global "My Team New Score" - ', myteam_new_score)
	# print('[VAR IN FUNC] Global "Other Team Current Score" - ', otherteam_current_score)
	# print('[VAR IN FUNC] Global "Other Team New Score" - ', otherteam_new_score)
	# print('[VAR IN FUNC] Global "Season" - ', season)
	# print('[VAR IN FUNC] Global "Gameday" - ', gameday)

try:
	# Set PID File (for Script Status in Front-End)
	pidManager()

	# Initialize Flow by Resetting All Variables
	reset_variables()

	localtz = dateutil.tz.tzlocal()
	team_id = get_team(config.teamName)
	print(dateLogger(), '[GLOBAL] Team Name = ', config.teamName)
	print(dateLogger(), '[GLOBAL] Team ID = ', team_id)

	print('')
	print('---------------------------------------')
	print('            MAIN CODE FLOW             ')
	print('---------------------------------------')

	while(1):
		if not season or not gameday:
			print(dateLogger(), '[MAIN] Today = ' + dateFormatter('%A, %B %d %Y'))
			# print(dateLogger(), '[MAIN] Season OR Gameday is False, Check Status...')
			season = is_season()
			gameday = is_game_today(team_id)

		if season:
			print(dateLogger(), "[MAIN] Hockey Season = Yes!")

			if gameday:
				print(dateLogger(), "[MAIN] Game Today = Yes!")

				game_info = get_game_info(team_id)
				game_info_status = game_info[1]
				print('')
				print(dateLogger(), '[MAIN] Game Status (In Loop) - ', game_info_status)

				if game_info_status == "Preview":
					countdown = game_info[0]
					if countdown < 0:
						print(dateLogger(), '[MAIN] Preview Past Game Time - Sleep for 60 seconds.')
						time.sleep(60)
					else:
						# If there is a game today, this function kicks off at midnight
						# Load a file in JSON Format for Front-End to Read
						front_end_gameinfo(game_info)

						print(dateLogger(), '[MAIN] Preview - Opponent - ' + game_info[4])
						print(dateLogger(), '[MAIN] Preview - Sleep for ', countdown, ' seconds')
						time.sleep(countdown)

				elif game_info_status == "Live":
					now = datetime.datetime.now()
					nowstring = now.strftime('%H:%M:%S')
					nowstring_log = '[', nowstring, ']'
					myteam_new_score = game_info[2]
					otherteam_new_score = game_info[3]
					other_team = game_info[4]

					# Logic to Check if Script Started in Middle of Game
					myteam_score_diff = myteam_new_score - myteam_current_score
					otherteam_score_diff = otherteam_new_score - otherteam_current_score

					if myteam_score_diff > 1:
						myteam_current_score = myteam_new_score

					if otherteam_score_diff > 1:
						otherteam_current_score = otherteam_new_score

					print('---------------------------------------')
					print(dateLogger(), '[MAIN] ', config.teamName, ' - Current Score - ', myteam_current_score)
					print(dateLogger(), '[MAIN] ', config.teamName, ' - New Score - ', myteam_new_score)
					print(dateLogger(), '[MAIN] ', other_team, ' - Current Score - ', otherteam_current_score)
					print(dateLogger(), '[MAIN] ', other_team, ' - New Score - ', otherteam_new_score)
					print('---------------------------------------')

					# Check for "Manually Set Goal Score"
					if getManualGoalStatus:
						if myteam_new_score > myteam_current_score:
							print(dateLogger(), '[GOAL] MY TEAM - Goal by the ' + team_name)
							print(dateLogger(), '[GOAL] Time of the Goal - ' + nowstring)
							myteam_new_score = myteam_current_score + 1

							if config.host_type == 'pi':
								LightsSounds.goalHorn('enable')
								LightsSounds.goalLights('enable')

								time.sleep(config.goal_time)
								LightsSounds.goalHorn('disable')
								LightsSounds.goalLights('disable')

					# Logic to Check if Score Has Changed
					# This shouldn't trigger the score twice since we updated the current score by 1
					if myteam_new_score > myteam_current_score:
						print(dateLogger(), '[GOAL] MY TEAM - Goal by the ' + team_name)
						print(dateLogger(), '[GOAL] Time of the Goal - ' + nowstring)
						myteam_current_score = myteam_new_score

						if config.host_type == 'pi':
							LightsSounds.goalHorn('enable')
							LightsSounds.goalLights('enable')

							time.sleep(config.goal_time)
							LightsSounds.goalHorn('disable')
							LightsSounds.goalLights('disable')

						# requests.get(ifttt)

					if otherteam_new_score > otherteam_current_score:
						print('[GOAL] OTHER TEAM - Goal by the ' + other_team)
						print('[GOAL] Time of the Goal - ' + nowstring)
						otherteam_current_score = otherteam_new_score

					time.sleep(5)

				elif game_info_status == "Final":
					# Reset All Variables (To Reset Loop)
					reset_variables()
					wait("day")

			else:
				print(dateLogger(), '[DEBUG] Game Today = No :(')
				wait("day")

		else:
			print(dateLogger(), '[DEBUG] Hockey Season = No :(')
			wait("season")

		print('---------------------------------------')

except KeyboardInterrupt:
	print('')
	print('---------------------------------------')
	print(dateLogger(), '[MAIN] Keyboard Interrupt Received - Cleaning Up...')

	if config.host_type == 'pi':
		# Restore GPIO to default state
		GPIO.cleanup()

	os.remove(pidfile)
