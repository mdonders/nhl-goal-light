#!/usr/local/bin/python3

import RPi.GPIO as GPIO
import time

# Pin Definitons:
relayPin = 17  # Broadcom pin 17 (P1 pin 11)
GPIO.setmode(GPIO.BCM)  # Broadcom pin-numbering scheme
GPIO.setup(relayPin, GPIO.OUT)

# Loop to blink our led
while True:
	GPIO.output(relayPin, GPIO.HIGH)
	print("On")
	time.sleep(1)
	GPIO.output(relayPin, GPIO.LOW)
	print("Off")
	time.sleep(1)


